import requests
import sys, os
import urllib
from time import sleep
from random import randint, seed
from bs4 import BeautifulSoup as bs

def scrape(link):
    print("Scraping",link,"...")
    savepath = "crawled/"
    if not os.path.exists("crawled"):
                os.makedirs("crawled")
    r = requests.get(link)
    r.encoding = "utf-8"
    if r.status_code != 200:
        print("Failed with code",r.status_code)
        return
    s = bs(r.text,"lxml")
    savepath = urllib.parse.urljoin(savepath,link.partition("//")[2].strip("/").replace("/","__"))
    if "Archive" not in link:
        with open(savepath,"w") as outfile:
            outfile.write(s.get_text())
    l = []
    links = s.find_all('a')
    stem = "".join(link.partition(".com")[:-1])
    print(stem)
    for link in links:
        try:
            nx = urllib.parse.urljoin(stem,link['href'])
            if ".com" in nx.partition(stem)[0]:
                continue
            if stem in nx and "#" not in nx and "search" not in nx and "share" not in nx and "tweet" not in nx:
                l.append(nx)
        except KeyError:
            continue
    return link, s, set(l) 

def crawl(start=None,visited=None,to_visit=None):
    if not visited:
        visited = []
    if not to_visit:
        to_visit = []
    if start:
        try:
            g, text, links = scrape(start)
            visited.append(start)
            for i in links:
                if i not in to_visit and i not in visited:
                    to_visit.append(i)
        except TypeError:
            print("")
    else:
        if len(to_visit) == 0:
            return
    for v in to_visit:
        try:
            g, text, links = scrape(v)
            visited.append(v)
            for i in links:
                if i not in to_visit and i not in visited:
                    to_visit.append(i)
        except (TypeError,OSError):
            continue

if __name__=="__main__":
    random.seed() # paranoid
    if len(sys.argv)<2:
        print ("Use: $ python3 sitelist.txt") 
        exit(1)
    try:
        sitelist = [x.strip("\n") for x in open(sys.argv[1]) if ".co" in x or ".org" in x or ".net" in x]
    except FileNotFoundError:
        print("Invalid file name.")
        exit(1)
    for item in sitelist:
        crawl(item)
        sleep(randint(1,5))
